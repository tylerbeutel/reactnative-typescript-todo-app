import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableWithoutFeedbackBase } from 'react-native';
import { Todo } from '../types/types';
import genTimestampKey from '../utils/genTimestampKey';

type Props = {
    addTodo: (newTodo: Todo) => void
}

const AddTodo:React.FC<Props> = ({ addTodo }) => {
    const [inputValue, setInputValue] = useState('');

    return (
        <View style={styles.container} >
            <Text style={styles.title}>What are your todos?</Text>
            <TextInput 
                style={styles.textInput}
                placeholderTextColor='#88f'
                value={inputValue}
                placeholder='Type todo then press enter...'
                onChangeText={(value) => setInputValue(value)}
                onSubmitEditing={() => addTodo({
                    title: inputValue,
                    complete: false,
                    todoKey: genTimestampKey()
                })}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        minHeight: 200,
        backgroundColor: 'blue',
        paddingVertical: 20,
        paddingHorizontal: 10
    },
    title: {
        marginTop: 40,
        textAlign: 'center',
        fontSize: 30,
        color: '#fff'
    },
    textInput: {
        margin: 10,
        padding: 10,
        color: '#fff',
        fontSize: 16,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 10,
    }
});


export default AddTodo;