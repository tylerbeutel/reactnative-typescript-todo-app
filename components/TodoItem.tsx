import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Alert } from 'react-native';


type TodoItemProps = {
    title: string;
    todoKey: number;
    complete: boolean;
    handleTodoPress: (key: number) => void;
    deleteTodo: (key: number) => void;
}

const TodoItem:React.FC<TodoItemProps> = ({ title, todoKey, complete, handleTodoPress, deleteTodo }) => {

    const handleTodoLongPress = () => {
        Alert.alert('Warning!', 'Are you Sure you want to delete this todo?', [
            { text: 'delete', onPress: () => deleteTodo(todoKey) },
            { text: 'cancel' },
        ])
    }

    return (
        <TouchableOpacity 
            key={todoKey}
            style={styles.todoContainer}
            onPress={() => handleTodoPress(todoKey)}
            onLongPress={handleTodoLongPress}
        >
            <Text style={styles.todoText}>
                <Text style={styles.icon}>{complete ? '😎': '🥺'} </Text>
                <Text style={ complete ? styles.complete : styles.todoText } > {title}</Text>
            </Text>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    todoContainer: {
        flexDirection: 'row',
        paddingVertical: 20,
        paddingHorizontal: 16,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    todoText: {
        fontSize: 20,
        color: '#000'
    },
    complete: {
        // fontSize: 20,
        textDecorationLine: 'line-through',
        color: '#aaa',
    },
    icon: {
        fontSize: 24
    }
});


export default TodoItem;