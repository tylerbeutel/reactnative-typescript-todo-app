namespace types {
    export interface TodoItem {
        title: string;
        key: string;
        complete: boolean;
    }
};