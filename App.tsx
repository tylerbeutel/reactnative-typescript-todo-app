import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import AddTodo from './components/AddTodo';
import TodoItem from './components/TodoItem';
import { Todo } from './types/types';


const App:React.FC = () => {

  const [todos, setTodos] = useState<{ [todoKey: number]: Todo }>({
    1 : { todoKey: 1, title: 'Add a Todo ', complete: false }
  });

  const handleTodoPress = (key: number) => {
    setTodos((prevTodos) => {
      return {
        ...prevTodos, 
        [key]: { ...prevTodos[key], complete: !prevTodos[key].complete }
      };
    });
  };

  const deleteTodo = (key: number) => {
    setTodos((prevTodos) => {
      let newTodos = {...prevTodos};
      delete newTodos[key];
      return newTodos;
    })
  }

  const addTodo = (newTodo: Todo) => {
    setTodos((prevTodos) => {
      return { ...prevTodos, [newTodo.todoKey]: newTodo };
    })
  }

  return (
    <View style={styles.container}>
      <StatusBar translucent style='light' />

      <AddTodo addTodo={addTodo} />

      <FlatList 
        data={Object.values(todos)}
        style={styles.todoList}
        renderItem = {({ item }) => (
          <TodoItem 
            title={item.title}
            key={item.todoKey}
            todoKey={item.todoKey}
            complete={item.complete}
            handleTodoPress={handleTodoPress}
            deleteTodo={deleteTodo}
          />
        )}
      />
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  todoList: {
    paddingHorizontal: 20,
    flex: 1
  }
});


export default App;