export type Todo = {
    todoKey: number;
    title: string;
    complete: boolean
};